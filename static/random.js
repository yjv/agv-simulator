window.onload = () => {
  let svg = document.getElementById('svg');
  let cars = [];
  let radius = 80;
  let startX = [];
  let u = 30;
  let speed = u / 1000 * 60;
  let width = 300;
  for (let i = 0; i <= 6; i++) {
    startX.push((width * i) + 80);
  }
  let y_interval = 200;
  let y_init = 20;
  for (let i = 0; i < 5; i++) {
    startX.forEach((x, j) => {
      let point1 = {x, y: y_init + 5 + (i * y_interval)};
      let point2 = {x: point1.x + 40, y: point1.y};
      let center1 = {x: point2.x, y: point2.y + radius};
      let point3 = {x: point2.x + radius, y: point2.y + radius };
      let center2 = {x: center1.x + radius * 2, y: center1.y};
      let paths = [
        new Line(point1.x, point1.y, point2.x, point2.y),
        new Arc(point2.x, point2.y, center1.x, center1.y, -Math.PI / 2),
        new Arc(point3.x, point3.y, center2.x, center2.y, Math.PI / 2),
      ];
      paths.forEach(path => path.appendTo(svg));
      let car = new Car(name(`${i}-${j}`), speed, x, y_init + (i * y_interval));
      car.registerPaths(paths);
      car.appendTo(svg);
      cars.push(car);
    });
  }

  setInterval(() => {
    cars.forEach(car => car.tick());
  }, 1000 / 60);
}

function name(id) {
  return `random_${id}`;
}
