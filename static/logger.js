let attempt_count = 1;
let loop_count = 1;
let reliability = 0;
let sensitivity = 0;
let always = 0;
let angle = 0;
let sample = 100;
let attempt = sample / 10;
let data_index = {
  "1": 1,
  "2": 1,
  "3": 1,
  "4": 1,
  "5": 1,
};
let RELIABILITY = [0.95, 0.9, 0.85];
let SENSITIVITY = [0.030, 0.025, 0.020];
let ALWAYS = ['on', 'off', 'random'];
let attempt_count_dom;
let loop_count_dom;
let start, restart, update_attempt_count, update_loop_count, update_reliability, update_sensitivity, update_always;
let Cars = [
  {
    "1": CarWithSensor1AlwaysOn,
    "2": CarWithSensor2AlwaysOn,
    "3": CarWithSensor3AlwaysOn,
    "4": CarWithSensor4AlwaysOn,
    "5": Car,
  },
  {
    "1": CarWithSensor1AlwaysOff,
    "2": CarWithSensor2AlwaysOff,
    "3": CarWithSensor3AlwaysOff,
    "4": CarWithSensor4AlwaysOff,
    "5": Car,
  },
  {
    "1": CarWithSensor1AlwaysRandom,
    "2": CarWithSensor2AlwaysRandom,
    "3": CarWithSensor3AlwaysRandom,
    "4": CarWithSensor4AlwaysRandom,
    "5": Car,
  }
]
window.onload = () => {
  attempt_count_dom = document.getElementById('attempt-count');
  loop_count_dom = document.getElementById('loop-count');
  reliability_dom = document.getElementById('reliability');
  sensitivity_dom = document.getElementById('sensitivity');
  always_dom = document.getElementById('always');
  start();
}

update_attempt_count = function() {
  if (attempt_count_dom) attempt_count_dom.innerHTML = attempt_count;
}
update_loop_count = function() {
  if (loop_count_dom) loop_count_dom.innerHTML = loop_count;
}
update_reliability = function() {
  if (reliability_dom) reliability_dom.innerHTML = RELIABILITY[reliability];
}
update_sensitivity = function() {
  if (sensitivity_dom) sensitivity_dom.innerHTML = SENSITIVITY[sensitivity];
}
update_always = function() {
  if (always_dom) always_dom.innerHTML = ALWAYS[always];
}

let interval;
start = () => {

  update_attempt_count();
  update_loop_count();
  update_reliability();
  update_sensitivity();
  update_always();

  let svg = document.getElementById('svg');
  svg.innerHTML = "";
  let cars = [];
  let radius = 80;
  let startY = [];
  let u = 30;
  let speed = u / 1000 * 60;
  let height = 200;
  for (let i = 0; i < 10; i++) {
    startY.push((height * i) + 5);
  }
  let x_interval = 300;
  let x_init = 80;
  let size = [10, 10, 10, 10, 10];
  startY.forEach((y, j) => {
    let sensor_id = j + 1;
    for (let i = 0; i < size[j]; i++) {
      let point1 = {x: x_init + radius + (i * x_interval), y: y + 5};
      let point2 = {x: point1.x + 40, y: point1.y};
      let center1 = {x: point2.x, y: point2.y + radius};
      let point3 = {x: point2.x, y: point2.y + radius * 2 };
      let point4 = {x: point3.x - 40, y: point3.y };
      let center2 = {x: point1.x, y: point1.y + radius};
      let paths = [
        new Line(point1.x, point1.y, point2.x, point2.y),
        new Arc(point2.x, point2.y, center1.x, center1.y, -Math.PI),
        new Line(point3.x, point3.y, point4.x, point4.y),
        new Arc(point4.x, point4.y, center2.x, center2.y, -Math.PI),
      ];
      paths.forEach(path => path.appendTo(svg));
      let car = new Cars[always][sensor_id](name(sensor_id, sample, i), speed, x_init + radius + (i * x_interval), y, angle, SENSITIVITY[sensitivity], RELIABILITY[reliability], folder_name());
      car.registerPaths(paths);
      car.appendTo(svg);
      cars.push(car);
    }
  });

  interval = setInterval(() => {
    cars.forEach(car => car.tick());
    if (!cars.map(car => car.speed).reduce((cur, speed) => {
      return cur + speed;
    })) {
      restart();
    }
  }, 1000 / 60);
}

restart = () => {
  clearInterval(interval);
  loop_count++;
  if (loop_count > 10) {
    loop_count = 1;
    for (let sample in data_index) {
      data_index[sample] = 1;
    }
    attempt_count++;
  }
  if (attempt_count <= attempt) {
    start();
  } else {
    attempt_count = 1;
    data_index = {
      "1": 1,
      "2": 1,
      "3": 1,
      "4": 1,
      "5": 1,
    };
    if (!angle) {
      angle = Math.PI;
      start();
    } else {
      angle = 0;
      if (next()) {
        start();
      } else {
        console.log('Done');
      }
    }
  }
}

function next() {
  reliability++;
  if (reliability >= 3) {
    reliability = 0;
    sensitivity++;
    if (sensitivity >= 3) {
      sensitivity = 0;
      always++;
      if (always >= 3) {
        return false;
      }
    }
  }
  return true;
}

function folder_name() {
  return `sample${sample}_r${reliability}_s${sensitivity}_a${always}`;
}

function name(id, n) {
  let index = (angle? n: 0) + data_index[id]++;
  return `sensor${id}_size_${('000' + n).substr(-3)}_attempt_${('000' + attempt_count).substr(-3)}_${('000' + index).substr(-3)}`;
}
