let attempt_count = 1;
let loop_count = 1;
let sensor_id = 1;
let angle = 0;
let data_index = {
  "10": 1, "100": 1, "250": 1,
};
let attempt_count_dom;
let loop_count_dom;
let start, restart, update_attempt_count, update_loop_count, update_sensor;
let Cars = {
  "1": CarWithSensor1AlwaysRandom,
  "2": CarWithSensor2AlwaysRandom,
  "3": CarWithSensor3AlwaysRandom,
  "4": CarWithSensor4AlwaysRandom,
}
window.onload = () => {
  attempt_count_dom = document.getElementById('attempt-count');
  loop_count_dom = document.getElementById('loop-count');
  sensor_dom = document.getElementById('sensor');
  start();
}

update_attempt_count = function() {
  if (attempt_count_dom) attempt_count_dom.innerHTML = attempt_count;
}
update_loop_count = function() {
  if (loop_count_dom) loop_count_dom.innerHTML = loop_count;
}
update_sensor = function() {
  if (sensor_dom) sensor_dom.innerHTML = sensor_id;
}

let interval;
start = () => {

  update_attempt_count();
  update_loop_count();
  update_sensor();

  let svg = document.getElementById('svg');
  svg.innerHTML = "";
  let cars = [];
  let radius = 80;
  let startY = [];
  let u = 30;
  let speed = u / 1000 * 60;
  let height = 200;
  for (let i = 0; i < 10; i++) {
    startY.push((height * i) + 5);
  }
  let x_interval = 300;
  let x_init = 80;
  let size = [1, 0, 5, 5, 0, 5, 5, 5, 5, 5];
  let sample = [10, 0, 100, 100, 0, 250, 250, 250, 250, 250];
  startY.forEach((y, j) => {
    for (let i = 0; i < size[j]; i++) {
      let point1 = {x: x_init + radius + (i * x_interval), y: y + 5};
      let point2 = {x: point1.x + 40, y: point1.y};
      let center1 = {x: point2.x, y: point2.y + radius};
      let point3 = {x: point2.x, y: point2.y + radius * 2 };
      let point4 = {x: point3.x - 40, y: point3.y };
      let center2 = {x: point1.x, y: point1.y + radius};
      let paths = [
        new Line(point1.x, point1.y, point2.x, point2.y),
        new Arc(point2.x, point2.y, center1.x, center1.y, -Math.PI),
        new Line(point3.x, point3.y, point4.x, point4.y),
        new Arc(point4.x, point4.y, center2.x, center2.y, -Math.PI),
      ];
      paths.forEach(path => path.appendTo(svg));
      let car = new Cars[sensor_id](name(sensor_id, sample[j], i), speed, x_init + radius + (i * x_interval), y, angle);
      car.registerPaths(paths);
      car.appendTo(svg);
      cars.push(car);
    }
  });

  interval = setInterval(() => {
    cars.forEach(car => car.tick());
    if (!cars.map(car => car.speed).reduce((cur, speed) => {
      return cur + speed;
    })) {
      restart();
    }
  }, 1000 / 60);
}

restart = () => {
  clearInterval(interval);
  loop_count++;
  if (loop_count > 10) {
    loop_count = 1;
    for (let sample in data_index) {
      data_index[sample] = 1;
    }
    attempt_count++;
  }
  if (attempt_count <= 10) {
    start();
  } else {
    attempt_count = 1;
    if (sensor_id < 4) {
      sensor_id++;
      start();
    } else {
      if (!angle) {
        angle = Math.PI;
        sensor_id = 1;
        start();
      } else {
        console.log('Done');
      }
    }
  }
}

function name(id, n) {
  let index = (angle? n: 0) + data_index[n]++;
  return `sensor${id}_random_size_${('0000' + n).substr(-4)}_set_attempt_${('000' + attempt_count).substr(-3)}_${('000' + index).substr(-3)}`;
}
