window.onload = () => {
  let svg = document.getElementById('svg');
  let cars = [];
  let radius = [];
  let startX = [];
  let speed = [];
  let u = 5;
  let width = 200;
  for (let i = 1; i <= 10; i++) {
    radius.push(10 * i);
    if (i % 2 == 1) {
      startX.push((width * i) + 10);
      speed.push(u * i * 60 / 1000);
    }
  }
  radius.forEach((radius, i) => {
    startX.forEach((x, j) => {
      let point1 = {x, y: 25 + (i * 100)};
      let point2 = {x: point1.x + 40, y: point1.y};
      let center1 = {x: point2.x, y: point2.y + radius};
      let point3 = {x: point2.x + radius, y: point2.y + radius };
      let center2 = {x: center1.x + radius * 2, y: center1.y};
      let paths = [
        new Line(point1.x, point1.y, point2.x, point2.y),
        new Arc(point2.x, point2.y, center1.x, center1.y, -Math.PI / 2),
        new Arc(point3.x, point3.y, center2.x, center2.y, Math.PI / 2),
      ];
      paths.forEach(path => path.appendTo(svg));
      let car = new Car(name(speed[j], radius), speed[j], x, 20 + (i * 100));
      car.registerPaths(paths);
      car.appendTo(svg);
      cars.push(car);
    });
  });

  setInterval(() => {
    cars.forEach(car => car.tick());
  }, 1000 / 60);
  // (new Arc(50, 50, 100, 100, Math.PI / 2)).appendTo(svg);
  // (new Arc(150, 50, 100, 100, Math.PI / 2)).appendTo(svg);
  // (new Arc(50, 150, 100, 100, Math.PI / 2)).appendTo(svg);
  // (new Arc(150, 150, 100, 100, Math.PI / 2)).appendTo(svg);
}

function name(speed, radius) {
  return `normal_s${speed}_cr${radius}`;
}
