class Line {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    let dot_size = Dot.getSize();
    let length = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    let angle = Math.atan((y2 - y1) / (x2 - x1));
    this.dots = [];
    for (let i = 0; i <= length; i += dot_size / 3 * 2) {
      this.dots.push(new Dot(x1 + i * Math.cos(angle), y1 + i * Math.sin(angle)));
    }
  }

  appendTo(svg) {
    this.path = document.createElementNS("http://www.w3.org/2000/svg", "path");
    this.path.setAttribute('d', `M${this.x1} ${this.y1} L${this.x2} ${this.y2}`);
    this.path.setAttribute('stroke', 'black');
    this.path.setAttribute('stroke-width', 4);
    svg.appendChild(this.path);
  }

  collide(x, y) {
    let collided = false;
    this.dots.forEach(dot => {
      collided |= dot.collide(x, y);
    });
    return collided;
  }
}
