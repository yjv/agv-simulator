class Arc {
  constructor(x1, y1, xc, yc, rad) {
    let dot_size = Dot.getSize();
    let radius = Math.sqrt(Math.pow(x1 - xc, 2) + Math.pow(y1 - yc, 2));
    let rad_interval = (dot_size / 3 * 2) / radius;
    let alpha = Math.atan((y1 - yc) / (x1 - xc));
    if (x1 - xc < 0) {
      alpha = Math.PI + alpha;
    }
    let direction = rad > 0;
    this.center = new Dot(xc, yc);
    this.dots = [];
    this.points = [];
    for(let i = 0; direction? i <= rad: i >= rad; i += (direction? 1: -1) * rad_interval) {
      let x = xc + radius * Math.cos(alpha - i);
      let y = yc + radius * Math.sin(alpha - i);
      this.dots.push(new Dot(x, y));
      this.points.push({x, y});
    }
    let x = xc + radius * Math.cos(alpha - rad);
    let y = yc + radius * Math.sin(alpha - rad);
    this.dots.push(new Dot(x, y));
    this.points.push({x, y});
  }

  appendTo(svg) {
    this.path = document.createElementNS("http://www.w3.org/2000/svg", "path");
    let d = this.points.map(({x, y}, i) => {
      if (i)
        return `L${x.toFixed(2)} ${y.toFixed(2)}`;
      return `M${x.toFixed(2)} ${y.toFixed(2)}`;
    }).join(' ');
    this.path.setAttribute('d', `${d}`);
    this.path.setAttribute('fill', 'none');
    this.path.setAttribute('stroke', 'black');
    this.path.setAttribute('stroke-width', 4);
    svg.appendChild(this.path);
    // this.dots.forEach(dot => dot.appendTo(svg));
    // this.center.appendTo(svg);
  }

  collide(x, y, size) {
    let collided = false;
    this.dots.forEach(dot => {
      collided |= dot.collide(x, y, size);
    });
    return collided;
  }
}
