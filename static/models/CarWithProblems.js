let start_to_fail = 2;
let stop_after = start_to_fail + 3;
class CarWithSensor1AlwaysOn extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [0, 1, 1, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 1;
  }
}

class CarWithSensor2AlwaysOn extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 0, 1, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 1;
  }
}

class CarWithSensor3AlwaysOn extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 1, 0, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 1;
  }
}

class CarWithSensor4AlwaysOn extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 1, 1, 0][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 1;
  }
}

class CarWithSensor1AlwaysRandom extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [0, 1, 1, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return Math.random() < 0.5;
  }
}

class CarWithSensor2AlwaysRandom extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 0, 1, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return Math.random() < 0.5;
  }
}

class CarWithSensor3AlwaysRandom extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 1, 0, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return Math.random() < 0.5;
  }
}

class CarWithSensor4AlwaysRandom extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 1, 1, 0][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return Math.random() < 0.5;
  }
}


class CarWithSensor1AlwaysOff extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [0, 1, 1, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 0;
  }
}

class CarWithSensor2AlwaysOff extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 0, 1, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 0;
  }
}

class CarWithSensor3AlwaysOff extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 1, 0, 1][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 0;
  }
}

class CarWithSensor4AlwaysOff extends Car {
  getSensorStatus(i) {
    if (this.ticked / 60 > start_to_fail) {
      return [1, 1, 1, 0][i];
    } else {
      return super.getSensorStatus(i);
    }
  }

  stopAfter() {
    return stop_after;
  }

  getBrokenSensorData(i) {
    return 0;
  }
}
