class Dot {
  static getSize() {
    return 4;
  }

  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.size = Dot.getSize();
  }

  getRadius() {
    return this.size / 2;
  }

  appendTo(svg) {
    this.circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    this.circle.setAttribute('cx', this.x);
    this.circle.setAttribute('cy', this.y);
    this.circle.setAttribute('stroke', 'black');
    this.circle.setAttribute('r', this.getRadius());
    svg.appendChild(this.circle);
  }

  collide(x, y) {
    let r = this.getRadius();
    if (
      (this.betweenX(x + Car.getScannerSize()) || this.betweenX(x - Car.getScannerSize())) &&
      (this.betweenY(y + Car.getScannerSize()) || this.betweenY(this.y > y - Car.getScannerSize()))
    ) {
      return true;
    }
    return false;
  }

  betweenX(x) {
    return x < this.x + this.size / 2 && x > this.x - this.size / 2;
  }

  betweenY(y) {
    return y < this.y + this.size / 2 && y > this.y - this.size / 2;
  }
}
