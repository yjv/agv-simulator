class Car {
  static getScannerSize() {
    return 2;
  }

  constructor(name, initSpeed = 5, x = 0, y = 0, angle = 0, sensitivity = 0.025, reliability = 0.9, folder = "") {
    this.folder = folder;
    this.name = name;
    this.angle = angle; // rad
    this.speed = initSpeed;
    this.size = 10;
    this.scanner_size = Car.getScannerSize();
    this.scanner_interval = 3;
    this.forward = 8;
    this.position = {};
    this.position.x = x;
    this.position.y = y;
    this.sensitivity = sensitivity;
    this.paths = [];
    this.logs = [];
    this.offtrack_count = 0;
    this.offtrack_count_limit_second = 0.5;
    this.reliability = reliability;
    this.ticked = 0;
  }

  stopAfter() {
    return 5;
  }

  getSensorStatus(i) {
    return [1, 1, 1, 1][i];
  }

  getBrokenSensorData(i) {
    return Math.random() < 0.5;
  }

  get velocity() {
    return {
      x: this.speed * Math.cos(this.angle),
      y: this.speed * Math.sin(this.angle)
    }
  }

  rotate(rad) {
    this.angle += rad;
  }

  tick() {
    this.ticked++;
    if (this.speed) {
      this.scan();
      this.position.x += this.velocity.x
      this.position.y += this.velocity.y;
      this.render();
    }
    if (this.stopAfter() && this.ticked / 60 > this.stopAfter()) {
      this.saveLogs();
      this.stop();
    }
  }

  scan() {
    let midpoint = (this.scanners.length - 1) / 2;
    let angle = 0;
    let result = this.scanners.map((scanner, i) => {
      let x = parseInt(scanner.getAttribute('x')) + parseInt(scanner.getAttribute('width')) / 2;
      let y = parseInt(scanner.getAttribute('y')) + parseInt(scanner.getAttribute('height')) / 2;
      let collided = false;
      this.paths.forEach(path => {
        collided |= path.collide(x, y);
      });
      if (this.getSensorStatus(i)) {
        // Sensor works
        if (Math.random() < this.reliability) {
          if (collided) return 1;
          return 0;
        // Random factor
        } else {
          return Math.random() < 0.5;
        }
      } else {
        // Sensor broken
        return this.getBrokenSensorData(i);
      }
    });
    let rear_result = this.rear_scanners.map((scanner, i) => {
      let x = parseInt(scanner.getAttribute('x')) + parseInt(scanner.getAttribute('width')) / 2;
      let y = parseInt(scanner.getAttribute('y')) + parseInt(scanner.getAttribute('height')) / 2;
      let collided = false;
      this.paths.forEach(path => {
        collided |= path.collide(x, y);
      });
      if (collided) return 1;
      return 0;
    });

    angle -= result[0]? this.sensitivity: 0;
    angle -= result[1]? this.sensitivity * 1.5: 0;
    angle += result[2]? this.sensitivity: 0;
    angle += result[3]? this.sensitivity * 1.5: 0;
    if (!result.reduce((cul, scanned) => cul || scanned) || !rear_result.reduce((cul, scanned) => cul || scanned)) {
      this.offtrack(!rear_result.reduce((cul, scanned) => cul || scanned));
    }
    else {
      this.record({ logs:[...result.map(sensor => !!sensor? 1:0), ...rear_result.map(sensor => !!sensor? 1:0)], angle: angle.toFixed(4) });
      this.rotate(angle);
      this.clearOfftrack();
    }
  }

  offtrack(stop = false) {
    this.offtrack_count++;
    if (this.offtrack_count / 60 > this.offtrack_count_limit_second) {
      if (stop) {
        this.saveLogs();
        this.stop();
      }
    }
  }

  clearOfftrack() {
    this.offtrack_count = 0;
  }

  stop() {
    this.speed = 0;
  }

  saveLogs() {
    axios({
      method: 'post',
      url: '/log',
      data: {
        logs: this.logs,
        name: this.getName(),
        folder: this.folder,
      }
    });
  }

  getName() {
    return this.name;
  }

  record(result) {
    this.logs.push(result);
    while(this.logs.length > 50) {
      this.logs.shift();
    }
  }

  registerPaths(paths) {
    this.paths = paths;
  }

  render() {
    let car_center_offset = (this.size / 2);
    let x = this.position.x + car_center_offset;
    let y = this.position.y + car_center_offset;
    this.rect.setAttribute('x', this.position.x);
    this.rect.setAttribute('y', this.position.y);
    this.rect.setAttribute('transform', `rotate(${this.angle / Math.PI * 180} ${x} ${y})`);

    let scanner_center_offset = (this.scanner_size / 2);
    let length = this.scanners.length - 1;
    this.scanners.forEach((scanner, i) => {
      let offset = i - length / 2;
      let _x = this.forward * Math.cos(this.angle) + -offset * this.scanner_interval * Math.sin(this.angle);
      let _y = this.forward * Math.sin(this.angle) + offset * this.scanner_interval * Math.cos(this.angle);
      let scanner_x = x + _x - scanner_center_offset;
      let scanner_y = y + _y - scanner_center_offset;
      scanner.setAttribute('x', scanner_x);
      scanner.setAttribute('y', scanner_y);
      scanner.setAttribute('transform', `rotate(${this.angle / Math.PI * 180} ${scanner_x + scanner_center_offset} ${scanner_y + scanner_center_offset})`);
    });
    this.rear_scanners.forEach((scanner, i) => {
      let offset = i - length / 2;
      let _x = this.forward * Math.cos(this.angle) + -offset * this.scanner_interval * Math.sin(this.angle);
      let _y = this.forward * Math.sin(this.angle) + offset * this.scanner_interval * Math.cos(this.angle);
      let scanner_x = x - _x - scanner_center_offset;
      let scanner_y = y - _y - scanner_center_offset;
      scanner.setAttribute('x', scanner_x);
      scanner.setAttribute('y', scanner_y);
      scanner.setAttribute('transform', `rotate(${this.angle / Math.PI * 180} ${scanner_x + scanner_center_offset} ${scanner_y + scanner_center_offset})`);
    });
  }

  appendTo(svg) {
    this.rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    this.rect.setAttribute('width', this.size);
    this.rect.setAttribute('height', this.size);
    this.rect.setAttribute('fill', 'black');
    this.scanners = [
      document.createElementNS("http://www.w3.org/2000/svg", "rect"),
      document.createElementNS("http://www.w3.org/2000/svg", "rect"),
      document.createElementNS("http://www.w3.org/2000/svg", "rect"),
      document.createElementNS("http://www.w3.org/2000/svg", "rect")
    ];
    this.scanners.forEach(scanner => {
      scanner.setAttribute('width', this.scanner_size);
      scanner.setAttribute('height', this.scanner_size);
      scanner.setAttribute('fill', 'blue');
      svg.appendChild(scanner);
    });
    this.rear_scanners = [
      document.createElementNS("http://www.w3.org/2000/svg", "rect"),
      document.createElementNS("http://www.w3.org/2000/svg", "rect"),
      document.createElementNS("http://www.w3.org/2000/svg", "rect"),
      document.createElementNS("http://www.w3.org/2000/svg", "rect")
    ];
    this.rear_scanners.forEach(scanner => {
      scanner.setAttribute('width', this.scanner_size);
      scanner.setAttribute('height', this.scanner_size);
      scanner.setAttribute('fill', 'blue');
      svg.appendChild(scanner);
    });
    svg.appendChild(this.rect);
    this.render();
  }
}
