const express = require('express')
const app = express()
const port = 3000
const fs = require('fs');
const os = require('os');
const { resolve } = require('path');

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.use(express.static('static'))

app.post('/log', function (req, res) {
  let path = resolve('./logs', `${req.body.name}.logs`);
  let folder = req.body.folder;
  if (folder) {
    if (!fs.existsSync(resolve('./logs', folder))) {
      fs.mkdirSync(resolve('./logs', folder));
    }
    path = resolve('./logs', folder, `${req.body.name}.logs`);
  }
  let data = req.body.logs.map(log => {
    return log.logs.join(',') + `,${log.angle}`
  }).join(os.EOL);
  fs.writeFileSync(path, data, 'utf-8');
  res.send("OK")
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
